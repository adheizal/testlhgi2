//
// xmega twi for as5510 linear magnetic encoder
// sec, 2016

#include "serial.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#define SLAVE_ADDRESS    0x56
#define CPU_SPEED       32000000

USART_data_t USART_data;

uint32_t value; //magnetic encoder reading
unsigned char Data_LSB, Data_MSB; //high byte and low byte of reading

int main(void) {
   // set up clock
   OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   //set up usart
   PORTD.DIRSET = PIN3_bm; //TXD0 where DIRSET sets a pin as an output
   PORTD.DIRCLR = PIN2_bm; //RXD0 where DIRCLR sets a pin as in input
   USART_InterruptDriver_Initialize(&USART_data, &USARTD0, USART_DREINTLVL_LO_gc);
   USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
   USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
   USART_Baudrate_Set(&USARTD0, 123 , -4); //230400 baud with .08% error
   USART_Rx_Enable(USART_data.usart);
   USART_Tx_Enable(USART_data.usart);
   //enable interrupts
   PMIC.CTRL |= PMIC_LOLVLEX_bm;

   //set up LED as output 
   PORTC.DIRSET = PIN0_bm;
   //setting an internal pull up resistor on pin6 for button
   PORTC.PIN4CTRL = PORT_OPC_PULLUP_gc;

   sei(); //set global interrupt enable


   while(1){
      PORTC.OUTSET = PIN0_bm;
      _delay_ms(100);
      PORTC.OUTCLR = PIN0_bm;
      _delay_ms(100);

      if(PORTC.IN & PIN4_bm){
         usart_send_byte(&USART_data,121);
         usart_send_byte(&USART_data,101);
         usart_send_byte(&USART_data,115);
         usart_send_byte(&USART_data,10);
         usart_send_byte(&USART_data,10); // translate byte using an ASCII table
      }
      else{
         usart_send_byte(&USART_data,110);
         usart_send_byte(&USART_data,111);
         usart_send_byte(&USART_data,10);
         usart_send_byte(&USART_data,10); // translate byte using an ASCII table
      }
   }
}

ISR(USARTD0_RXC_vect){USART_RXComplete(&USART_data);}
ISR(USARTD0_DRE_vect){USART_DataRegEmpty(&USART_data);}









