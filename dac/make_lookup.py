#!/usr/bin/env python
from math import *

a = 2048-1
r = 128
f = lambda x: sin(2*pi*x/r)

print [int(a*f(ti))+a for ti in range(r)]
