//
// hello.dac.8E5.c
//
// 8E5 dac
//

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>


uint16_t lookup[128] = {
2047, 2147, 2247, 2347, 2446, 2544, 2641, 2736, 2830, 2922, 3011, 3099, 3184, 3266, 3345, 3421, 3494, 3563, 3629, 3691, 3749, 3802, 3852, 3897, 3938, 3974, 4005, 4032, 4054, 4071, 4084, 4091, 4094, 4091, 4084, 4071, 4054, 4032, 4005, 3974, 3938, 3897, 3852, 3802, 3749, 3691, 3629, 3563, 3494, 3421, 3345, 3266, 3184, 3099, 3011, 2922, 2830, 2736, 2641, 2544, 2446, 2347, 2247, 2147, 2047, 1947, 1847, 1747, 1648, 1550, 1453, 1358, 1264, 1172, 1083, 995, 910, 828, 749, 673, 600, 531, 465, 403, 345, 292, 242, 197, 156, 120, 89, 62, 40, 23, 10, 3, 0, 3, 10, 23, 40, 62, 89, 120, 156, 197, 242, 292, 345, 403, 465, 531, 600, 673, 749, 828, 910, 995, 1083, 1172, 1264, 1358, 1453, 1550, 1648, 1747, 1847, 1947
};

int main(void) {
   //
   // main
   //
   uint16_t angle=0;
   //
   // set up clock
   //
   //OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   //while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   //CCP = CCP_IOREG_gc; // enable protected register change
   //CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   OSC.PLLCTRL = OSC_PLLFAC4_bm | OSC_PLLFAC3_bm; // 2 MHz * 24 = 48 MHz
   OSC.CTRL = OSC_PLLEN_bm; // enable PLL
   while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL

   //set up dac
   DACA.CTRLB = ( DACA.CTRLB & ~DAC_CHSEL_gm ) | DAC_CHSEL_DUAL_gc;
   DACA.CTRLC = ( DACA.CTRLC & ~( DAC_REFSEL_gm | DAC_LEFTADJ_bm ) ) | DAC_REFSEL_AVCC_gc;
   DACA.CTRLA = ( DACA.CTRLA & ~DAC_CH1EN_bm ) | DAC_CH0EN_bm | DAC_ENABLE_bm;
   while(1){
	DACA.CH0DATA = lookup[angle];
	angle = (angle+1)%128;
   }
}
