//
// hello.ftdi.16A4.qdec.c
//
// 16A4 quadrature decoder with 115200 baud FTDI position and frequency report
//
#include <stdio.h>
#include "serial.h"

#define serial_port PORTD.OUT
#define serial_direction PORTD.DIR
#define serial_pins PORTD.IN
#define serial_pin_in PIN6_bm
#define serial_pin_out PIN7_bm

#define led_port PORTD.OUT
#define led_direction PORTD.DIR
#define led_pin PIN4_bm

#define CLOCK_DIV_bm  TC_CLKSEL_DIV64_gc
#define CLOCK_DIV     64

int main(void) {
   static char buffer[50] = {0};
   uint16_t calcFreq     = 0;
   uint16_t calcPos = 0;
   char calcDir = 0;
   // set up clock
   OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   uint16_t lineCount = 1200; //number of lines in encoder

   // initialize output pins
	PORTD.DIRCLR = (0x03<<0); //set up port d pin 0 as quad input
	PORTCFG.MPCMASK = (0x03<<0);    /* Set QDPH0 and QDPH1 sensing level. */
	PORTD.PIN0CTRL = (PORTD.PIN0CTRL & ~PORT_ISC_gm) | PORT_ISC_LEVEL_gc;
	/* Configure event channel 0 for quadrature decoding of pins. */
	EVSYS.CH0MUX = EVSYS_CHMUX_PORTD_PIN0_gc;
	EVSYS.CH0CTRL = EVSYS_QDEN_bm | EVSYS_DIGFILT_2SAMPLES_gc;
		
	//if(useIndex){
		/*  Configure event channel 1 as index channel. Note
		 *  that when enabling Index in channel n, the channel
		 *  n+1 must be configured for the index signal.*/
	//	EVSYS.CH1MUX = EVSYS_CHMUX_PORTD_PIN0_gc + 2;
	//	EVSYS.CH1CTRL = EVSYS_DIGFILT_2SAMPLES_gc;
	//	EVSYS.CH0CTRL |= (uint8_t) qIndexState | EVSYS_QDIEN_bm;
	//}
	/* Configure TC as a quadrature counter. */
	TCC0.CTRLD = (uint8_t) TC_EVACT_QDEC_gc | TC_EVSEL_CH0_gc;
	TCC0.PER = lineCount - 1;
	TCC0.CTRLA = TC_CLKSEL_DIV1_gc;	

	/* Configure channel 2 to input pin for freq calculation. */
	EVSYS.CH2MUX = EVSYS_CHMUX_PORTD_PIN0_gc;
	EVSYS.CH2CTRL = EVSYS_DIGFILT_4SAMPLES_gc;
	/* Configure TC to capture frequency. */
	TCD0.CTRLD = (uint8_t) TC_EVACT_FRW_gc | TC_EVSEL_CH2_gc;
	TCD0.PER = 0xFFFF;
	TCD0.CTRLB = TC0_CCAEN_bm;
	TCD0.CTRLA = CLOCK_DIV_bm;

   set(serial_port, serial_pin_out);
   output(serial_direction, serial_pin_out);
   output(led_direction, led_pin);
   
  while (1) {
      while ((TCD0.INTFLAGS & TC0_CCAIF_bm) ==  0){}
      calcPos = TCC0.CNT;
      calcDir = (TCC0.CTRLFSET & TC0_DIR_bm);	
      calcFreq = (F_CPU / CLOCK_DIV) / (TCD0.CCA & 0xFFFC);	
      sprintf(buffer,"x, dx: %d, %s%d", calcPos, calcDir ? " ":"-", calcFreq);
      put_string(&serial_port, serial_pin_out, buffer);
      put_char(&serial_port, serial_pin_out, 10); // new line
      set(led_port, led_pin);
      _delay_ms(1);
      clear(led_port, led_pin);
      }
   }



