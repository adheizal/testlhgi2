//
// watt.8e5.c
//
// open and closed loop current measurement with xmega 8e5 and drv5053 hall effect sensor
//
#include <stdio.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include "serial.h"
#include "adc.h"

#define output(directions,pin) (directions |= pin) // set port direction for output
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin

#define serial_port PORTD.OUT
#define serial_direction PORTD.DIR
#define serial_pins PORTD.IN
#define serial_pin_in PIN6_bm
#define serial_pin_out PIN7_bm

#define sec_port PORTD.OUT
#define sec_direction PORTD.DIR
#define sec_pin PIN0_bm

#define lc_port PORTA.OUT
#define lc_direction PORTA.DIR
#define lc_pos_pin PIN5_bm
#define lc_gnd_pin PIN4_bm

volatile int64_t ADC_result, last_ADC_result, d_result;
int16_t offset;

static char buffer[50] = {0};

int main(void) {
	// set up clock
	//
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock
	//set up sleep
	set_sleep_mode(SLEEP_MODE_IDLE);	
 
	//set up serial
	set(serial_port, serial_pin_out);
	output(serial_direction, serial_pin_out);

	//set up load cell
   //output(lc_direction, lc_pos_pin);
   //output(lc_direction, lc_gnd_pin);
   //set(lc_port,lc_pos_pin);
   //clear(lc_port,lc_gnd_pin);
	//_delay_ms(100);


	//calibrate ADC to reduce nonlinearity
	ADCA.CALL = SP_ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL0_offset );
	ADCA.CALH = SP_ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL1_offset );
	ADC_ConvMode_and_Resolution_Config(&ADCA, 1, ADC_RESOLUTION_12BIT_gc);
	//ADC_BandgapReference_Enable(&ADCA);
	ADC_Reference_Config(&ADCA, ADC_REFSEL_INTVCC_gc);
	//_delay_ms(200);
	ADC_Prescaler_Config(&ADCA, ADC_PRESCALER_DIV32_gc);
	ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH0,
	                                 ADC_CH_INPUTMODE_DIFFWGAINL_gc,
	                                 ADC_CH_GAIN_16X_gc);

	//ADC_Ch_InputMux_Config(&ADCA.CH0, ADC_CH_MUXPOS_PIN6_gc, ADC_CH_MUXNEGL_PIN3_gc);
	ADC_Ch_InputMux_Config(&ADCA.CH0, ADC_CH_MUXPOS_PIN1_gc, ADC_CH_MUXNEGL_PIN2_gc);
	ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;

	ADC_Enable(&ADCA);
	ADC_Wait_8MHz(&ADCA);	
	offset = ADC_Offset_Get_Signed(&ADCA, &(ADCA.CH0), 100);

	last_ADC_result = 0;
	int n_samples = 100; //oversampling number
	int alpha = 1; //low pass filter constant.  larger -> slower

	sleep_enable();
	int sleep=0;
	while (1) {
		ADC_result = 0;
		for(int i=0; i<n_samples; i++){
			ADC_Ch_Conversion_Start(&ADCA.CH0);
			if (sleep){
				PMIC.CTRL = PMIC_HILVLEN_bm;
				sei();
				sleep_cpu();
			} else{
				while(!ADC_Ch_Conversion_Complete(&ADCA.CH0)){};	
				ADC_result +=  ADC_ResultCh_GetWord_Signed(&ADCA.CH0, offset);  
			}

		}
		ADC_result = ADC_result/n_samples;
		last_ADC_result += (ADC_result-last_ADC_result)/alpha;
    

		sprintf(buffer,"1234,%ld", last_ADC_result);
		put_string(&serial_port, serial_pin_out, buffer);
		put_char(&serial_port, serial_pin_out, 10); // new line
		_delay_ms(5);
	}
	return 0;
}

ISR(ADCA_CH0_vect)
{
	ADC_result +=  ADC_ResultCh_GetWord_Signed(&ADCA.CH0, offset);  
	ADCA.CH0.INTFLAGS = ADC_CH_CHIF_bm; 
}

